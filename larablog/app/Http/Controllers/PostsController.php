<?php
/**
 * Enpoints to do
 *      Create /blog/create POST
 *      Read /blog GET
 *      Single Post /blog/id GET
 *      Update /blog/{slug} PUT
 *      Delete /blog/{id} DELETE
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Cviebrock\EloquentSluggable\Services\SlugService;

class PostsController extends Controller
{

    public function __construct() {
        $this->middleware('auth',  [
            'except' => [
                'index',
                'show'
            ]
            ]);
    }
    public function index() {
        //  dd($post);
        return view('blog.index')
        ->with('posts', Post::orderBy('created_at', 'DESC')->get());
    }

    // rendering the create page
    public function create() {
        return view('blog.create');
    }

    // creating the post
    public function store(Request $request) {
        // dd($request->title);

        $request -> validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpg,png,jpeg|max:5048'
            ]);
        
            $newImageName = uniqid(). '-' . $request->title . '.' . $request->image->extension();
            // dd($newImageName);
            $request->image->move(public_path('images'), $newImageName);

            // $slug = SlugService::createSlug(Post::class, 'slug', $request->title);
            // dd($slug);

            Post::create([
              'title' => $request->input('title'),
              'description' => $request->input('description'),
              'slug' => SlugService::createSlug(Post::class, 'slug', $request->title),
              'image_path' => $newImageName,
              'user_id' => auth()->user()->id

            ]);

            return redirect ('/blog')->with('message', 'Your post has been created!');
    }

    public function show($slug){
        return view('blog.show')->with('post', Post::where('slug', $slug)->first());
    }

    public function edit ($slug) {
        return view('blog.edit')->with('post', Post::where('slug', $slug)->first());
    }

    public function update (Request $request, $slug) {
        Post::where('slug', $slug)->update([
            'title' => $request->input('title'),
              'description' => $request->input('description'),
              'slug' => SlugService::createSlug(Post::class, 'slug', $request->title),
              'user_id' => auth()->user()->id
        ]);

        return redirect('/blog')->with(
            'message', 'Post successfully updated.'
        );
    }

    public function destroy($slug) {
        $post = Post::where('slug', $slug);
        $post->delete();

        return redirect('/blog')->with('message', 'Post deleted successfully!');
    }
}






