@extends('layouts.app')

@section('content')
    <div class="background-image grid gridcols-1 m-auto">
        <div class="flex text-gray-100 pt-10">
            <div class="m-auto pt-4 pb-16 sm:m-auto w-4/5 block text-center">
               <div class="m-auto pt-4 pb-16 sm:auto w-4/5 block text-center">
               <h1 class="sm:text-white text-5xl uppercase font-bold text-shadow-md pb-14">Are you a laravel developer?</h1>
                    <a href="/blog" class="text-center bg-gray-50 text-gray-700 py-2 px-4 font-bold text-xl uppercase">Read More</a>
                </div>
            </div>
        </div>
    </div>
    <div class="sm:grid grid-cols-2 gap-20 w-4/5 mx-auto py-15 border-b border-gray-200">
        <div>
            <img src="https://cdn.pixabay.com/photo/2017/08/07/19/45/ecommerce-2607114_960_720.jpg" width="700" alt="">
        </div>
        <div class="m-auto sm:m-auto text-left w-4/5 block">
            <h2 class="text-3xl font-extrabold text-gray-600"> How eCommerce has changed our lives </h2>
            <p class="py-8 text-gray-500 text-s">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam molestie est et mi consectetur lacinia. Curabitur laoreet, lacus sed fermentum ornare, tortor ligula elementum ex, at vestibulum ipsum justo a leo. Vivamus auctor viverra neque, non luctus ante faucibus eget. Curabitur id odio laoreet, varius ligula sit amet, elementum ipsum. 
            </p>


            <a href="/blog" class="uppsercase bg-blue-500 text-gray-100 text-s font-extrabold py-3 px-8 rounded-3xl" >Read More</a>
        </div>
    </div>

    <div class="text-center p-15 bg-black text-white">
       
            <h2 class="text-2xl pb-5 text-l"> Get more knowledge, visit our blog page.</h2>
            <span class="font-extrabold block text-4xl py-1">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam molestie est et mi consectetur lacinia.
</span>


            
       
    </div>

    <div class="text-center py-15">
        <span class="uppercase text-s text-gray-400">Latest Blog</span>
        <h2 class="text-4xl font-bold py-10">Recent Posts</h2>
        <p class="m-auto w-4/5 text-gray-500"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam molestie est et mi consectetur lacinia. </p>
    </div>

    <div class="sm:grid grid-cols-2 w-4/5 m-auto">
        <div class="flex bg-yellow-700 text-gray-100 pt-10">
            <div class="m-auto pt-4 pb-16 sm:m-auto w-4/5 block">
                <span class="uppercase text-xs">Laravel</span>
                <h3 class="text-xl font-bold py-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam molestie est et mi consectetur lacinia.</h3>
                <a href ="" class="uppercase bg-transparent border-2 border-gray-100 text-gray-100 text-xs font-extrabold py-3 px-5 rounded-3xl">Read More</a>
            </div>
        </div>
        <div>
            <img src="https://cdn.pixabay.com/photo/2013/08/11/19/46/coffee-171653_960_720.jpg" alt="">
        </div>
    </div>
@endsection