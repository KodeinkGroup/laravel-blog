<footer class="bg-gray-800  py-20 mt-20">
    <div class = "sm:grid grid-cols-3 w-4/5 pb-10 m-auto border-b-2 border-gray-700">
        <div>
            <h3 class="text-l sm:font-bold text-gray-100">About Blog</h3>
             <p class="mt-4 text-gray-400">Lorem Ipsum dolor manor te dim palor</p>
        </div>
        <div>
            <h3 class="text-l sm:font-bold text-gray-100">Quick Links</h3>
            <ul class="py-4 sm:text-s pt-4 text-gray-400" >
                <li class="pb-1">
                    <a href="/blog">Blog</a>
                </li>
                <li class="pb-1">
                    <a href="/login">Login</a>
                </li>
                <li class="pb-1">
                    <a href="/register">Register</a>
                </li>
            </ul>
        </div>
        <div>
            <h3 class="text-l sm:font-bold text-gray-100">Latest Articles</h3>
            <ul class="py-4 sm:text-s pt-4 text-gray-400">
                <li class="pb-1">
                    <a href="/blog">Why you should code.</a>
                </li>
                <li class="pb-1">
                    <a href="/login">Wordpress or Shopify?</a>
                </li>
                <li class="pb-1">
                    <a href="/register">Why coding is important.</a>
                </li>
            </ul>
        </div>
    </div>
    <p class="w-25 w-4/5 pb-3 m-auto text-xa text-gray-100 pt-6">Lehlohonolo Motsoeneng, Copyright 2021. All rights reserved</p>
</footer>